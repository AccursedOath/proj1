import org.junit.Test;

import static org.junit.Assert.*;

public class PogingTest {

    @Test
    public void addPunten() {
        Vraag een = new Vraag("test","D");
        Examen examen = new Examen("topo",een);
        Student student = new Student("David",102L);
        Poging opt = new Poging(student,examen);
        opt.addPunten(20);
        opt.addPunten(100);
        opt.addPunten(80);
        opt.addPunten(0);
        assertEquals((Integer)200,opt.getPunten());
    }

    @Test
    public void behaalt() {
        Vraag een = new Vraag("test","D");
        Examen examen = new Examen("topo",een);
        Student student = new Student("David",102L);
        Poging opt = new Poging(student,examen);
        opt.addPunten(100);
        assertTrue(opt.behaalt());

    }

    @Test
    public void Nietbehaalt() {
        Vraag een = new Vraag("test","D");
        Examen examen = new Examen("topo",een);
        Student student = new Student("David",102L);
        Poging opt = new Poging(student,examen);
        opt.addPunten(50);
        assertFalse(opt.behaalt());

    }

    @Test
    public void getIsGeslaagd() {
        Vraag een = new Vraag("test","D");
        Examen examen = new Examen("topo",een);
        Student student = new Student("David",102L);
        Poging opt = new Poging(student,examen);
        opt.addPunten(60);
        opt.behaalt();
        assertTrue(opt.getIsGeslaagd());

    }

    @Test
    public void getIsgefaald() {
        Vraag een = new Vraag("test","D");
        Examen examen = new Examen("topo",een);
        Student student = new Student("David",102L);
        Poging opt = new Poging(student,examen);
        opt.addPunten(20);
        opt.behaalt();
        assertFalse(opt.getIsGeslaagd());

    }

}
