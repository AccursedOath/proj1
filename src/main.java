import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

class Student{
    private String naam;
    private Long studentnummer;
    private  ArrayList<Examen> isGeslaagd;

    public Student(String naam, Long studentnummer){
        this.naam = naam;
        this.studentnummer = studentnummer;
        this.isGeslaagd = new ArrayList<>();
    }


    public String getNaam() {
        return naam;
    }

    public Long getStudentnummer() {
        return studentnummer;
    }

    public void isGeslaagd(Examen examen) {
        isGeslaagd.add(examen);
    }

    public ArrayList<Examen> SlaagLijst(){
        return isGeslaagd;
    }

}

class Examen{
    private String naam;
    private Integer slagingspunten = 55;
    private ArrayList<Vraag> bestaatUit; //zometeen

    public Examen(String naam, Vraag vraag){
        this.naam = naam;
        bestaatUit = new ArrayList<>();
        bestaatUit.add(vraag);
    }

    public String getNaam() {
        return naam;
    }
    public void addVraag(Vraag vraag){
        bestaatUit.add(vraag);
    }

    public ArrayList<Vraag> getBestaatUit() {
        return bestaatUit;
    }

    public Integer getSlagingspunten() {
        return slagingspunten;
    }
}

class Vraag {

    private String vraag;
    private String antwoord;

    public Vraag(String vraag, String antwoord){
        this.vraag = vraag;
        this.antwoord = antwoord;
    }

    public String getVraag() {
        return this.vraag;
    }

    public String getAntwoord() {
        return this.antwoord;
    }

    public void setVraag(String vraag) {
        this.vraag = vraag;
    }

    public void setAntwoord(String antwoord) {
        this.antwoord = antwoord;
    }

    public Boolean checkAntwoord(String antwoord){
        return antwoord.equals(this.antwoord);
    }

}

class Poging {
    private Boolean isGeslaagd;
    private int punten;
    private Student student;
    private Examen examen;

    public Poging(Student student, Examen examen){
        this.student = student;
        this.examen = examen;
    }

    public void addPunten(Integer punten){
        this.punten += punten;
    }

    public Integer getPunten(){
        return this.punten;
    }

    public Boolean getIsGeslaagd(){
        return isGeslaagd;
    }

    public Boolean behaalt(){
        this.isGeslaagd = this.punten > examen.getSlagingspunten();
        return this.punten > examen.getSlagingspunten();
    }
} //getest

class Main {

    private static void DisplayExamens(ArrayList<Examen> examens) {
        /**
        for (Examen e : examens){
            System.out.println(e.getNaam());
        }
         */
        for(int i = 0; i < examens.size(); i++){
            Examen temp = examens.get(i);
            System.out.println((i+1)+ ") " + temp.getNaam());
        }
    }

    public static void Menu(){
        System.out.println("----kies uit het volgende----");
        System.out.println("1) Lijst met examens");
        System.out.println("2) Lijst met studenten");
        System.out.println("3) Nieuwe student inschrijven");
        System.out.println("4) Student verwijderen");
        System.out.println("5) Examen afnemen");
        System.out.println("6) Is student geslaagd voor test?");
        System.out.println("7) Welke examens heeft student gehaald?");
        System.out.println("8) Welke student heeft de meeste examens gehaald?");
        System.out.println("0) Exit");
        System.out.print("Uw keuze:");
        System.out.println();
        System.out.println("----------");
    }




    public static void main(String[] args) {
        Boolean begin =  true;
        Scanner scanner = new Scanner(System.in);
        ArrayList<Student> studenten = new ArrayList<>();
        ArrayList<Examen> examens = new ArrayList<>();
        Vraag reken = new Vraag("1 Wat is de hoofdstad van roemenie?\n" +
                "A boekarest\n" +
                "B Alba\n" +
                "C arad\n" +
                "D Salaj\n","A");
        Examen rekenen = new Examen("Reken Examen",reken);
        Vraag een = new Vraag("1 Wat is de hoofdstad van roemenie?\n" +
                "A boekarest\n" +
                "B Alba\n" +
                "C arad\n" +
                "D Salaj\n","A");
        Examen topo = new Examen("Topografie Examen",een);
        examens.add(rekenen);
        examens.add(topo);
        Vraag vraag = new Vraag("eerste vraag","D");

        MaakTopoExamen(topo);




        while (begin) {
            Menu(); // weergave menu
            int keuze = scanner.nextInt(); //keuzes inlezen van menu


            switch(keuze){
                case 0:
                    begin = false;
                    break;
                case 1:
                    DisplayExamens(examens);
                    break;
                case 2:
                    if (studenten.size()==0) {
                        System.out.println("Op dit moment heeft niemand zich ingeschreven!");
                    }
                    else {
                        DisplayStudentenLijst(studenten);
                    }
                    break;
                case 3:
                    scanner.nextLine();
                    StudentInschrijven(scanner, studenten);
                    break;
                case 4:
                    DisplayStudentenLijst(studenten);
                    System.out.println("wat is de studentennummer van de student dat u wilt verwijderen?");
                    scanner.nextLine();
                    Long snummer = scanner.nextLong();
                    int teller = 0;

                    for(Student s : studenten) {
                        if (snummer == s.getStudentnummer()){
                            System.out.println(s.getNaam() + " is verwijderd uit de studentenlijst.");
                            studenten.remove(teller);
                            break;
                        }
                        teller++;
                    }
                    break;

                case 5:
                    //kijken welke student deze toets maakt
                    //vragen of je je all hebt ingeschreven
                    Student examenkanidaat = new Student(null,null);
                    Boolean isIngeschreven = false; //checken of de student is ingeschreven

                    while (!isIngeschreven) {
                        System.out.println("Ben je all ingeschreven als student?");
                        System.out.println("1)Ik sta all ingeschreven");
                        System.out.println("2)Ik moet me nog inschrijven");
                        System.out.print("uw keuze:");
                        int ingeschreven = scanner.nextInt();
                        System.out.println(ingeschreven);

                        switch (ingeschreven) {
                            case 1:
                                System.out.println("Welke student ben je? kies uit deze lijst:");
                                DisplayStudentenLijst(studenten);
                                int StudentKeuze = scanner.nextInt();
                                System.out.println("0) Terug");

                                if (StudentKeuze > 0) {
                                    examenkanidaat = studenten.get(StudentKeuze - 1);
                                    isIngeschreven = true;
                                    break;
                                }
                                else {
                                    break;
                                }

                            case 2 :
                                scanner.nextLine();
                                StudentInschrijven(scanner, studenten);
                                break;
                        }
                    }

                    int totalepunten = 0;
                    System.out.println("welke examen zou je willen maken?");
                    DisplayExamens(examens);
                    scanner.nextLine();
                    int examenkeuze = scanner.nextInt();
                    scanner.nextLine();

                    if(examenkeuze == 2){       //topografie examen
                        //we willen elke vraag zien van de arraylist binnen een class
                        for (Vraag e : topo.getBestaatUit()){
                            System.out.println(e.getVraag());
                            System.out.print("Uw antwoord:");
                            String antwoord = scanner.nextLine();
                            if (e.checkAntwoord(antwoord)){
                                totalepunten += 500;
                            }
                        }
                        System.out.println("u heeft :" + totalepunten + " punten gescoord");
                        if(totalepunten >= topo.getSlagingspunten()){
                            examenkanidaat.isGeslaagd(topo);
                            System.out.println("gefeliciteerd u bent geslaagd voor deze Examen!");
                        }
                    }

                    break;
                case 6:
                    DisplayStudentenLijst(studenten);
                    System.out.println("welke student ben je?");
                    int studentkeuze = scanner.nextInt();
                    Student x = studenten.get(studentkeuze - 1);
                    for (Examen e : x.SlaagLijst()){
                        System.out.println(x.getNaam() + " is geslaagd voor de " +  e.getNaam() + "!");
                    }
                    break;


                    //test cases-------------------------------------------------------------
                case 10:
                    try {
                        File bestand = new File("recources\\stu.txt");
                        Scanner bestandlezer = new Scanner(bestand);

                        while(bestandlezer.hasNextLine()){
                            System.out.println(bestandlezer.nextLine());
                        }
                        bestandlezer.close();
                    }
                    catch (IOException e) {
                        System.out.println(e);
                    }
                    break;

                case 11:
                    try {
                        File bestand = new File("recources\\stu.txt");
                        //PrintWriter writer = new PrintWriter(bestand);
                        PrintWriter writer = new PrintWriter(new FileWriter(bestand, true));
                        for (int i = 0; i < studenten.size(); i++){
                            Student temp = new Student(null, null);
                            temp = studenten.get(i);
                            writer.println(temp.getNaam() + " Studentennummer: " + temp.getStudentnummer());
                        }

                        writer.close();
                    }
                    catch (IOException e) {
                        System.out.println(e);
                    }
                    break;

            }


        }
    }

    private static void DisplayStudentenLijst(ArrayList<Student> studenten) {
        for (int i = 0;i < studenten.size();i++)
        {
            Student temp = new Student("null",null);
            temp = studenten.get(i);
            System.out.print((i+1) + ") Naam: " + temp.getNaam() + " Studentennummer: " + temp.getStudentnummer());
            System.out.println();
        }
    }

    private static void StudentInschrijven(Scanner scanner, ArrayList<Student> studenten) {
        System.out.print("voer naam in:");
        String naam = scanner.nextLine();
        System.out.print("voer hier studentennummer in:");
        Long studentennummer = scanner.nextLong();
        Student nieuw = new Student(naam, studentennummer);
        studenten.add(nieuw);
        System.out.println("Welkom " + naam + " je staat ingeschreven met studentennummer: " + studentennummer);
    }

    private static void MaakTopoExamen(Examen topo) {
        //topo.addVraag(een);
        Vraag twee = new Vraag("2 In welke land ligt de stad Sofia?\n" +
                "A Italie\n" +
                "B Zwitserland\n" +
                "C Bulgarije -\n" +
                "D Hongarije\n","C");
        topo.addVraag(twee);
    }

}